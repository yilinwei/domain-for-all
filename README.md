# Setup

This project is set up with `sbt`. Run with `run` or `test`.

# File structure

 - Models for types are found in `types.scala` and expressions in `expr.scala`.
 - Evaluator is found in `eval.scala`
 - Typer is found in `typer.scala`
 - The untyped DSL is found in `pet.scala`
 - The typed DSL is found in `fish.scala`
 - Examples of the different typing scenarios can be found in `TyperSpec`

# Interesting resources

 - [The Most Beautiful Program Ever Written, Will Byrd](https://youtu.be/OyfBQmvr2Hc)
 - [The Essence of Scala, Martin Odersky](https://www.scala-lang.org/blog/2016/02/03/essence-of-scala.html)
 - [`typeof` type guards, Typescript](https://www.typescriptlang.org/docs/handbook/advanced-types.html#typeof-type-guards)
 - [The Idris Programming Language, Edwin Brady](https://github.com/edwinb/Idris2)

# Licensing

The code is licensed under GPLv3 (see `build.sbt`).
