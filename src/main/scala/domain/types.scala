package domain

sealed trait Type {

  final def union(b: Type): Type = {
    val set = (this, b) match {
      case (Type.Coprod(as), Type.Coprod(bs)) =>
        as ++ bs
      case (Type.Coprod(as), b) =>
        as + b
      case (a, Type.Coprod(bs)) =>
        bs + a
      case (a, b) =>
        Set(a, b)
    }
    if (set.size == 1) {
      set.head
    } else {
      Type.Coprod(set)
    }
  }

  final def subsetOf(other: Type): Boolean = {
    (this, other) match {
      case (Type.Coprod(tpes), Type.Coprod(otherTpes)) =>
        tpes.map(otherTpes.contains).foldLeft(true)(_ && _)
      case (t, Type.Coprod(otherTpes)) =>
        otherTpes.contains(t)
      case (tpe, otherTpe) =>
        tpe == otherTpe
    }
  }

  final def minus(other: Type): Option[Type] = {
    (this, other) match {
      case (Type.Coprod(types), Type.Coprod(otherTypes)) =>
        Some(Type(types -- otherTypes))
      case (Type.Coprod(types), tpe) =>
        Some(Type(types - tpe))
      case (tpe, otherType) =>
        if (tpe == otherType) {
          None
        } else {
          Some(tpe)
        }
    }
  }

  final def intersect(other: Type): Option[Type] = {
    def f(coprod: Type.Coprod, tpe: Type): Option[Type] = {
      if (coprod.types.contains(tpe)) {
        Some(tpe)
      } else {
        None
      }
    }
    (this, other) match {
      case (Type.Coprod(tpes), Type.Coprod(otherTypes)) =>
        val intersection = tpes.filter(otherTypes.contains)
        val len          = intersection.size
        if (len > 0) {
          if (len == 1) {
            Some(intersection.head)
          } else {
            Some(Type.Coprod(intersection))
          }
        } else {
          None
        }
      case (types @ Type.Coprod(_), otherType) =>
        f(types, otherType)
      case (tpe, types @ Type.Coprod(_)) =>
        f(types, tpe)
      case (tpe, otherType) =>
        if (tpe == otherType) {
          Some(tpe)
        } else {
          None
        }
    }
  }

}

object Type {

  def apply(types: Set[Type]): Type = {
    if (types.size > 1) {
      Type.Coprod(types)
    } else {
      types.head
    }
  }

  case object Num                              extends Type
  case object Str                              extends Type
  case object Bool                             extends Type
  case class Func(args: List[Type], ret: Type) extends Type
  case class Coprod(types: Set[Type])          extends Type
  case class Record(name: String)              extends Type

}
