package domain

object Typer {

  final class Context(val value: Map[Expr, Type]) {
    val lookup = value.get _

    def plus(bound: Bound): Context = {
      new Context(this.value ++ bound.values)
    }

    def plus(context: Context): Context = {
      new Context(this.value ++ context.value)
    }

    override def toString: String = value.toString

  }

  object Context {
    val default: Context = new Context(
      Map(
        Expr.plus    -> Type.Func(List(Type.Num, Type.Num), Type.Num),
        Expr.concat  -> Type.Func(List(Type.Str, Type.Str), Type.Str),
        Expr.len     -> Type.Func(List(Type.Str), Type.Num),
        Expr.boolTpe -> Type.Bool,
        Expr.numTpe  -> Type.Num,
        Expr.ttrue   -> Type.Bool,
        Expr.ffalse  -> Type.Bool,
        Expr.lt      -> Type.Func(List(Type.Num, Type.Num), Type.Bool),
        Expr.gt      -> Type.Func(List(Type.Num, Type.Num), Type.Bool)
      )
    )

    def apply(values: (Expr, Type)*) = {
      new Context(Map.from(values))
    }
  }

  final class Bound(val values: Map[Expr, Type]) {

    def or(bound: Bound): Bound = {
      new Bound(this.values.map {
        case (k, v) =>
          k -> bound.values
            .get(k)
            .map(v.union)
            .getOrElse(v)
      })
    }

    def and(bound: Bound): Either[Result, Bound] = {
      val types = bound.values ++ this.values
      val newTypes = types
        .foldLeft[Either[Result, List[(Expr, Type)]]](Right(List.empty)) {
          (b, a) =>
            a match {
              case (expr, boundType) => {
                b.flatMap { tail =>
                  bound.values
                    .get(expr)
                    .map(
                      otherBoundType =>
                        boundType
                          .intersect(otherBoundType)
                          .map((expr, _))
                          .map(Right(_))
                          .getOrElse(
                            Left(Result.fail("could not find union", expr))
                          )
                    )
                    .getOrElse(Right(expr -> boundType))
                    .map(_ :: tail)
                }
              }
            }
        }
      newTypes.map(_.toMap).map(new Bound(_))
    }

    def complement(typer: Expr => Result): Either[Result, Bound] = {
      values
        .map {
          case (expr, boundType) =>
            typer(expr).value
              .flatMap {
                case (tpe, _) =>
                  tpe
                    .minus(boundType)
                    .map(Right(_))
                    .getOrElse(Left(("could not find complement"), expr))
              }
              .map((expr, _))
        }
        .foldLeft[Either[(String, Expr), List[(Expr, Type)]]](Right(List.empty)) {
          (b, a) =>
            for {
              accumulation <- b
              result       <- a
            } yield result :: accumulation
        }
        .map(_.toMap)
        .left
        .map {
          case (message, expr) => Result.fail(message, expr)
        }
        .map(new Bound(_))
    }

    override def toString: String = values.toString
  }

  object Bound {
    val empty = new Bound(Map.empty)

    def single(expr: Expr, tpe: Type): Bound =
      new Bound(Map((expr, tpe)))

  }

  final class Result(val value: Either[(String, Expr), (Type, Bound)]) {

    final def modify(f: ((Type, Bound)) => Result): Result = {
      new Result(value.flatMap(f.andThen(_.value)))
    }

    final def isSuccess: Boolean = value.isRight

    final def isFailure: Boolean = value.isLeft

    final override def toString: String = value.toString

  }

  object Result {

    def success(tpe: Type): Result =
      success(tpe, Bound.empty)

    def success(tpe: Type, bound: Bound): Result =
      new Result(Right((tpe, bound)))

    def fail(error: String, expr: Expr) =
      new Result(Left((error, expr)))

  }

  def constructType(tpe: Expr.Type): Type = tpe match {
    case Expr.numTpe     => Type.Num
    case Expr.boolTpe    => Type.Bool
    case Expr.strTpe     => Type.Str
    case Expr.Type(name) => Type.Record(name)
  }

  def apply(expr: Expr, context: Context): Result = {
    val result = expr match {
      case n @ Expr.Lit.Num(_) =>
        Result.success(Type.Num)
      case s @ Expr.Lit.Str(_) =>
        Result.success(Type.Str)
      case sym @ Expr.Sym(symbol) =>
        context
          .lookup(sym)
          .map(Result.success _)
          .getOrElse(Result.fail(s"could not find $symbol in context", expr))
      case Expr.Apply(Expr.is, expr :: (typeExpr @ Expr.Type(_)) :: Nil) =>
        val boundTpe = constructType(typeExpr)
        apply(expr, context).modify {
          case (inferredTpe, _) =>
            if (boundTpe.subsetOf(inferredTpe)) {
              Result.success(Type.Bool, Bound.single(expr, boundTpe))
            } else {
              Result.fail(s"$inferredTpe is not subset of $boundTpe", expr)
            }
        }
      case Expr.Apply(Expr.not, cond :: Nil) => {
        apply(cond, context)
          .modify {
            case (Type.Bool, bound) =>
              bound
                .complement(expr => apply(expr, context))
                .map(Result.success(Type.Bool, _))
                .merge
            case _ => Result.fail("expected boolean", expr)
          }
      }
      case Expr.Apply(Expr.and, a :: b :: Nil) => {
        apply(a, context)
          .modify {
            case (Type.Bool, ba) =>
              apply(b, context.plus(ba))
                .modify {
                  case (Type.Bool, bb) =>
                    ba.and(bb).map(Result.success(Type.Bool, _)).merge
                  case _ => Result.fail("expected boolean", expr)
                }
            case _ => Result.fail("expected boolean", expr)
          }
      }
      case Expr.Apply(Expr.or, a :: b :: Nil) => {
        apply(a, context)
          .modify {
            case (Type.Bool, ba) =>
              ba.complement(expr => apply(expr, context))
                .map { complement =>
                  apply(b, context.plus(complement))
                    .modify {
                      case (Type.Bool, bb) =>
                        Result.success(Type.Bool, ba.or(bb))
                      case _ => Result.fail("expected boolean", expr)
                    }
                }
                .merge
            case _ => Result.fail("expected boolean", expr)
          }
      }
      case Expr.Apply(Expr.iff, cond :: thn :: els :: Nil) => {
        apply(cond, context)
          .modify {
            case (Type.Bool, bound) =>
              apply(thn, context.plus(bound))
                .modify {
                  case (thnTpe, _) =>
                    bound
                      .complement(expr => apply(expr, context))
                      .map { complement =>
                        apply(els, context.plus(complement)).modify {
                          case (elsTpe, _) =>
                            Result.success(thnTpe.union(elsTpe))
                        }
                      }
                      .merge
                }
            case _ => Result.fail("if takes bool condition", expr)
          }
      }

      case expr @ Expr.Apply(sym @ Expr.Sym(symbol), actualArgs) =>
        apply(sym, context)
          .modify {
            case (Type.Func(args, ret), _) =>
              args
                .map(Some.apply)
                .zipAll(actualArgs.map(Some.apply), None, None)
                .foldRight(Result.success(ret)) { (a, b) =>
                  if (b.isFailure) {
                    b
                  } else {
                    a match {
                      case (Some(expectedType), Some(term)) =>
                        apply(term, context)
                          .modify {
                            case (inferredType, _) =>
                              if (inferredType.subsetOf(expectedType)) {
                                Result.success(ret)
                              } else {
                                Result.fail(
                                  s"inferred type $inferredType instead of $expectedType",
                                  expr
                                )
                              }
                          }
                      case (None, _) =>
                        Result.fail("too few args", expr)
                      case (_, None) =>
                        Result.fail("too many args", expr)
                    }
                  }
                }
          }
      case _ => Result.fail("Unrecognized form", expr)
    }
    context
      .lookup(expr)
      .map { boundTpe =>
        result.modify {
          case (tpe, bound) =>
            boundTpe
              .intersect(tpe)
              .map(Result.success _)
              .getOrElse(
                Result.fail(s"$tpe and $bound have no intersection", expr)
              )
        }
      }
      .getOrElse(result)
  }
}
