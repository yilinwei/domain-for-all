package domain

sealed trait NewPet

object NewPet {

  case class Dog(name: String, age: Double, legs: Double) extends NewPet
  case class Cat(name: String, age: Double, legs: Double) extends NewPet
  case class Fish(name: String, age: Double)              extends NewPet

  val nameSymbol = Expr.Sym("name")
  val ageSymbol  = Expr.Sym("age")
  val legSymbol  = Expr.Sym("legs")

  val defaultEvalContext = Eval.Context.default.plus(
    new Eval.Context(
      Map(
        nameSymbol -> ((pet: NewPet) => {
          pet match {
            case Dog(name, _, _) => name
            case Cat(name, _, _) => name
            case Fish(name, _)   => name
          }
        }),
        ageSymbol -> ((pet: NewPet) => {
          pet match {
            case Dog(_, age, _) => age
            case Cat(_, age, _) => age
            case Fish(_, age)   => age
          }
        }),
        legSymbol -> ((pet: NewPet) => {
          pet match {
            case Dog(_, _, legs) => legs
            case Cat(_, _, legs) => legs
          }
        }),
        Expr.is -> ((x: Any, tpe: Type) => {
          tpe match {
            case Type.Record("Fish") => x.isInstanceOf[NewPet.Fish]
            case Type.Record("Dog")  => x.isInstanceOf[NewPet.Dog]
            case Type.Record("Cat")  => x.isInstanceOf[NewPet.Cat]
            case Type.Num            => x.isInstanceOf[Double]
            case Type.Str            => x.isInstanceOf[String]
            case _                   => false
          }
        })
      )
    )
  )

  val petSymbol = Expr.Sym("pet")

  def evalContext(pet: NewPet): Eval.Context = {
    new Eval.Context(
      Map(
        petSymbol -> pet
      )
    ).plus(defaultEvalContext)
  }

  val dogType  = Type.Record("Dog")
  val catType  = Type.Record("Cat")
  val fishType = Type.Record("Fish")
  val petType  = Type.Coprod(Set(dogType, catType, fishType))

  val typerContext = Typer.Context.default.plus(
    new Typer.Context(
      Map(
        nameSymbol -> Type.Func(List(petType), Type.Str),
        ageSymbol  -> Type.Func(List(petType), Type.Num),
        legSymbol -> Type
          .Func(List(Type.Coprod(Set(dogType, catType))), Type.Num),
        petSymbol -> petType
      )
    )
  )

}
