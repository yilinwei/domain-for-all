package domain

object Eval {

  final class Context(private val value: Map[Expr.Sym, Any]) {
    def lookup[A](sym: Expr.Sym): Option[A] =
      value.get(sym).map(_.asInstanceOf[A])
    def plus(context: Context): Context = {
      new Context(this.value ++ context.value)
    }
  }

  object Context {
    val default: Context = new Context(
      Map(
        Expr.plus   -> ((x: Double, y: Double) => x + y),
        Expr.concat -> ((x: String, y: String) => x + y),
        Expr.len    -> ((x: String) => x.length()),
        Expr.ttrue  -> true,
        Expr.ffalse -> false,
        Expr.not    -> ((x: Boolean) => !x),
        Expr.eq     -> ((x: Any, y: Any) => x.equals(y)),
        Expr.lt     -> ((x: Double, y: Double) => x < y),
        Expr.gt     -> ((x: Double, y: Double) => x > y)
      )
    )
  }

  type Result[A] = Either[Expr, A]

  def apply[A](expr: Expr, context: Context): Result[A] = {
    expr match {
      case Expr.Type(value)    => Right(Type.Record(value).asInstanceOf[A])
      case Expr.Lit.Num(value) => Right(value.asInstanceOf[A])
      case Expr.Lit.Str(value) => Right(value.asInstanceOf[A])
      case sym @ Expr.Sym(_) =>
        context
          .lookup[A](sym)
          .map(Right(_))
          .getOrElse(Left(sym))
      case Expr.Apply(Expr.iff, cond :: thn :: els :: Nil) =>
        apply[Boolean](cond, context)
          .flatMap { cond =>
            if (cond) {
              apply[A](thn, context)
            } else {
              apply[A](els, context)
            }
          }
      case Expr.Apply(Expr.and, x :: y :: Nil) =>
        apply[Boolean](x, context)
          .flatMap { cond =>
            if (cond) {
              apply[A](y, context)
            } else {
              Right(false.asInstanceOf[A])
            }
          }
      case Expr.Apply(Expr.or, x :: y :: Nil) =>
        apply[Boolean](x, context)
          .flatMap { cond =>
            if (cond) {
              Right(true.asInstanceOf[A])
            } else {
              apply[A](y, context)
            }
          }
      case Expr.Apply(sym, args) => {
        val len = args.length
        len match {
          case 1 =>
            for {
              f <- apply[Any => Any](sym, context)
              a <- apply[Any](args(0), context)
            } yield f(a).asInstanceOf[A]
          case 2 =>
            for {
              f <- apply[(Any, Any) => Any](sym, context)
              a <- apply[Any](args(0), context)
              b <- apply[Any](args(1), context)
            } yield f(a, b).asInstanceOf[A]
        }
      }
    }
  }
}
