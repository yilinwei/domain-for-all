package domain

sealed trait Pet

object Pet {

  case class Dog(name: String, age: Double) extends Pet
  case class Cat(name: String, age: Double) extends Pet

  val nameSymbol = Expr.Sym("name")
  val ageSymbol  = Expr.Sym("age")

  val defaultEvalContext = Eval.Context.default.plus(
    new Eval.Context(
      Map(
        nameSymbol -> ((pet: Pet) => {
          pet match {
            case Dog(name, _) => name
            case Cat(name, _) => name
          }
        }),
        ageSymbol -> ((pet: Pet) => {
          pet match {
            case Dog(_, age) => age
            case Cat(_, age) => age
          }
        })
      )
    )
  )

  val petSymbol = Expr.Sym("pet")

  def evalContext(pet: Pet): Eval.Context = {
    new Eval.Context(
      Map(
        petSymbol -> pet
      )
    ).plus(defaultEvalContext)
  }

}
