package domain

sealed trait Expr

object Expr {

  sealed trait Lit                             extends Expr
  case class Sym(value: String)                extends Expr
  case class Apply(sym: Sym, args: List[Expr]) extends Expr
  case class Type(name: String)                extends Expr

  val iff     = Expr.Sym("if")
  val is      = Expr.Sym("is")
  val and     = Expr.Sym("and")
  val or      = Expr.Sym("or")
  val not     = Expr.Sym("not")
  val plus    = Expr.Sym("+")
  val concat  = Expr.Sym("concat")
  val len     = Expr.Sym("len")
  val eq      = Expr.Sym("eq")
  val lt      = Expr.Sym("lt")
  val gt      = Expr.Sym("gt")
  val boolTpe = Expr.Sym("Bool")
  val numTpe  = Expr.Type("Num")
  val strTpe  = Expr.Type("Str")
  val ttrue   = Expr.Sym("true")
  val ffalse  = Expr.Sym("false")

  def apply1(sym: Expr.Sym, a: Expr): Expr = {
    Expr.Apply(sym, List(a))
  }

  def apply2(sym: Expr.Sym, a: Expr, b: Expr): Expr = {
    Expr.Apply(sym, List(a, b))
  }

  def apply3(sym: Expr.Sym, a: Expr, b: Expr, c: Expr): Expr = {
    Expr.Apply(sym, List(a, b, c))
  }

  object Is {
    def apply(expr: Expr, tpe: Expr.Type): Expr = {
      apply2(is, expr, tpe)
    }
  }

  object Or {
    def apply(x: Expr, y: Expr): Expr = {
      apply2(or, x, y)
    }
  }

  object If {
    def apply(cond: Expr, els: Expr, thn: Expr): Expr = {
      apply3(iff, cond, els, thn)
    }
  }

  object Not {
    def apply(cond: Expr): Expr = {
      apply1(not, cond)
    }
  }

  object And {
    def apply(x: Expr, y: Expr): Expr = {
      apply2(and, x, y)
    }
  }

  object Lit {
    case class Num(value: Double) extends Lit
    case class Str(value: String) extends Lit
  }

}
