package domain

object Parser {

  type Result = Either[String, Expr]

  def apply(text: String): Result = {
    def error = Left(text)
    val expr = text.headOption.map {
      case '(' if text.last == ')' => {
        def go(
            text: String,
            stack: String,
            depth: Int
        ): Either[String, List[Expr]] = {
          if (!text.isEmpty()) {
            val index = text.indexWhere(c => c == '(' || c == ')' || c == ' ')
            if (index >= 0) {
              text(index) match {
                case ' ' if depth == 0 =>
                  for {
                    expr <- apply(text.slice(0, index).trim())
                    exprs <- go(
                      text.slice(index + 1, text.length).trim(),
                      stack,
                      0
                    )
                  } yield expr :: exprs
                case '(' => {
                  go(
                    text.slice(index + 1, text.length).trim(),
                    stack + text.slice(0, index + 1),
                    depth + 1
                  )
                }
                case ')' if depth == 1 => {
                  for {
                    expr <- apply(stack + text.slice(0, index + 1))
                    exprs <- go(
                      text.slice(index + 1, text.length).trim(),
                      "",
                      0
                    )
                  } yield expr :: exprs
                }
                case ' ' => {
                  go(
                    text.slice(index + 1, text.length).trim(),
                    stack + text.slice(0, index + 1),
                    depth
                  )
                }
                case ')' =>
                  go(
                    text.slice(index + 1, text.length).trim(),
                    stack + text.slice(0, index + 1),
                    depth - 1
                  )
              }
            } else {
              apply(text).map(List(_))
            }
          } else if (text.isEmpty() && stack.trim().isEmpty()) {
            Right(List())
          } else {
            Left(text)
          }
        }
        go(text.slice(1, text.length - 1), "", 0)
          .flatMap {
            case (sym @ Expr.Sym(_)) :: exprs => Right(Expr.Apply(sym, exprs))
            case _                            => Left(text)
          }
      }
      case '"' =>
        text.lastOption
          .map { l =>
            if (l == '"') {
              Right(Expr.Lit.Str(text.slice(1, text.length - 1)))
            } else {
              error
            }
          }
          .getOrElse(error)
      case c if c.isDigit                          => Right(Expr.Lit.Num(text.toDouble))
      case c if c.isUpper && text.indexOf(' ') < 0 => Right(Expr.Type(text))
      case c if text.indexOf(' ') < 0              => Right(Expr.Sym(text))
      case _                                       => error
    }
    expr.getOrElse(error)
  }
}
