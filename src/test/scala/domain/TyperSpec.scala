package domain

import org.scalatest._

class TyperSpec extends FlatSpec with Matchers with Inside {
  "The typer" should "type if statements" in {
    val x = Expr.Sym("x")
    val context = Typer.Context.default.plus(
      Typer.Context(
        x -> Type.Coprod(Set(Type.Num, Type.Str))
      )
    )
    val expr = Expr.If(
      Expr.Is(x, Expr.numTpe),
      Expr.Apply(
        Expr.plus,
        List(x, Expr.Lit.Num(1))
      ),
      Expr.Apply(
        Expr.concat,
        List(x, Expr.Lit.Str("ace"))
      )
    )
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Coprod(Set(Type.Num, Type.Str))
    }
  }

  it should "type not statements" in {
    val x = Expr.Sym("x")
    val context = Typer.Context.default.plus(
      Typer.Bound.single(x, Type.Coprod(Set(Type.Num, Type.Str)))
    )
    val expr = Expr.If(
      Expr.Not(
        Expr.Is(x, Expr.numTpe)
      ),
      Expr.Apply(
        Expr.concat,
        List(x, Expr.Lit.Str("ace"))
      ),
      Expr.Apply(
        Expr.plus,
        List(x, Expr.Lit.Num(1))
      )
    )
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Coprod(Set(Type.Num, Type.Str))
    }
  }

  it should "type or statements" in {
    val x         = Expr.Sym("x")
    val f         = Expr.Sym("f")
    val g         = Expr.Sym("g")
    val recordTpe = Type.Record("moo")
    val context = Typer.Context.default.plus(
      Typer.Context.apply(
        x -> Type.Coprod(Set(Type.Num, Type.Str, recordTpe)),
        f -> Type.Func(List(Type.Coprod(Set(Type.Num, Type.Str))), Type.Num),
        g -> Type.Func(List(recordTpe), Type.Num)
      )
    )
    val expr = Expr.If(
      Expr.Or(
        Expr.Is(x, Expr.numTpe),
        Expr.Is(x, Expr.strTpe)
      ),
      Expr.Apply(f, List(x)),
      Expr.Apply(g, List(x))
    )

    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Num
    }
  }

  it should "type or guards" in {
    val x = Expr.Sym("x")
    val f = Expr.Sym("f")
    val context = Typer.Context.default.plus(
      Typer.Context(
        x -> Type.Coprod(Set(Type.Num, Type.Str)),
        f -> Type.Func(List(Type.Str), Type.Bool)
      )
    )
    val expr   = Expr.Or(Expr.Is(x, Expr.numTpe), Expr.Apply(f, List(x)))
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Bool
    }
  }

  it should "type and guards" in {
    val x = Expr.Sym("x")
    val f = Expr.Sym("f")
    val context = Typer.Context.default.plus(
      Typer.Context(
        x -> Type.Coprod(Set(Type.Num, Type.Str)),
        f -> Type.Func(List(Type.Str), Type.Bool)
      )
    )
    val expr   = Expr.And(Expr.Is(x, Expr.strTpe), Expr.Apply(f, List(x)))
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Bool
    }
  }

  it should "type and statements" in {
    val x = Expr.Sym("x")
    val y = Expr.Sym("y")
    val f = Expr.Sym("f")
    val g = Expr.Sym("g")
    val context = Typer.Context.default.plus(
      Typer.Context(
        x -> Type.Coprod(Set(Type.Num, Type.Str)),
        y -> Type.Coprod(Set(Type.Num, Type.Str)),
        f -> Type.Func(List(Type.Str, Type.Str), Type.Num),
        g -> Type.Func(List(Type.Num, Type.Num), Type.Num)
      )
    )
    val expr = Expr.If(
      Expr.And(Expr.Is(x, Expr.strTpe), Expr.Is(y, Expr.strTpe)),
      Expr.Apply(f, List(x, y)),
      Expr.Apply(g, List(x, y))
    )
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Num
    }

  }

  it should "type nested if statements" in {
    val x = Expr.Sym("x")
    val context = Typer.Context.default.plus(
      Typer.Bound
        .single(x, Type.Coprod(Set(Type.Num, Type.Str, Type.Record("moo"))))
    )

    val expr = Expr.If(
      Expr.Is(x, Expr.numTpe),
      Expr.Apply(
        Expr.plus,
        List(x, Expr.Lit.Num(1))
      ),
      Expr.If(
        Expr.Is(x, Expr.strTpe),
        Expr.Apply(
          Expr.len,
          List(x)
        ),
        Expr.Lit.Num(1)
      )
    )
    val result = Typer.apply(expr, context)
    inside(result.value) {
      case Right((tpe, _)) =>
        tpe shouldBe Type.Num
    }
  }
}
