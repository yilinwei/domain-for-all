package domain

import org.scalatest._

class EvalSpec extends FlatSpec with Matchers with Inside {
  "The evaluator" should "eval statements" in {
    val expr = Expr.Apply(
      Expr.plus,
      List(
        Expr.Lit.Num(1),
        Expr.Lit.Num(2)
      )
    )
    val result = Eval[Double](expr, Eval.Context.default)
    inside(result) {
      case Right(value) =>
        value shouldBe 3.0
    }
  }

  it should "eval boolean statements" in {
    val expr = Expr.And(
      Expr.ttrue,
      Expr.Or(
        Expr.ttrue,
        Expr.ffalse
      )
    )
    val result = Eval[Boolean](expr, Eval.Context.default)
    inside(result) {
      case Right(value) =>
        value shouldBe true
    }
  }
}
