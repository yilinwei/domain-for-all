package domain

import org.scalatest._

class ParserSpec extends FlatSpec with Matchers with Inside {

  "Parser" should "parse statements" in {
    val text   = "(a b c d)"
    val result = Parser(text)
    inside(result) {
      case Right(value) =>
        value shouldBe Expr.Apply(
          Expr.Sym("a"),
          List(
            Expr.Sym("b"),
            Expr.Sym("c"),
            Expr.Sym("d")
          )
        )
    }
  }

  it should "parse nested statements" in {
    val text   = "(a (b c) d)"
    val result = Parser(text)
    inside(result) {
      case Right(value) =>
        value shouldBe Expr.Apply(
          Expr.Sym("a"),
          List(
            Expr.Apply(
              Expr.Sym("b"),
              List(
                Expr.Sym("c")
              )
            ),
            Expr.Sym("d")
          )
        )
    }
  }

  it should "parse complex statements" in {
    val text   = """(eq (name pet) "fido")"""
    val result = Parser(text)
    val predicate = Expr.Apply(
      Expr.eq,
      List(
        Expr.Apply(
          Expr.Sym("name"),
          List(Expr.Sym("pet"))
        ),
        Expr.Lit.Str("fido")
      )
    )
    inside(result) {
      case Right(value) =>
        value shouldBe predicate
    }
  }
}
