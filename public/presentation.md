class: center, middle

# Do fish have legs?

.center[Yilin Wei]
.center[@Yilin47276121]
.center[<https://gitlab.com/yilinwei/domain-for-all>]

---

# Problem

```https
POST /pets/search?name=fido HTTP/1.1
```
--
```https
POST /pets/search?name=fido_AND_age=3 HTTP/1.1
```
--
```https
POST /pets/search?name=fido_AND_age<3_AND_age>5 HTTP/1.1
```
--
```https
POST /pets/search?name=fido_AND_age<3_AND_age>5_OR_name=whiskers HTTP/1.1
```
--
.center[**And so on...**]
---
# All roads lead to ~~Rome~~ Lisp
> Any sufficiently complicated C or Fortran program contains an ad-hoc,
informally-specified, bug-ridden, slow implementation of half of Common Lisp.

.right[***10th rule, Philip Greenspun***]

--

> Any sufficiently complicated ~~C or Fortran~~ **scala** program contains an ~~ad-hoc~~,
	~~informally-specified~~ **formally specified**, ~~bug-ridden~~ **bug-free**, ~~slow~~
	implementation of half of Common Lisp.

.right[***10th rule (revisited)***]

---

# Lisp

```lisp
(println "This is the total: %i" (+ a b))
```

```lisp
(and true (or false true))
```

```lisp
(f x y)
```

```lisp
(eq (name pet) "fido")
```

.center[**Simple syntax**]

---

# AST

```scala

sealed trait Expr

object Expr {
	sealed trait Lit extends Expr
	object Lit {
	  //Literals
	  case class Num(value: Double) extends Lit
	  case class Str(value: String) extends Lit
	}
	//Symbol
	case class Sym(value: String) extends Expr
	//Function application
	case class Apply(sym: Sym, args: List[Expr]) extends Expr
}

```
--
```scala
  Apply(
	Sym("eq"),
	List(
	  Apply(
		Sym("name"),
		List(Sym("pet"))
	  ),
	  Lit.Str("fido")
	  )
  )
```

---

# Evaluation

```scala

object Eval {
	def apply[A](expr: Expr): A = {
	  expr match {
		case Lit.Num(value) => value.asInstanceOf[A]
		case Lit.Str(value) => value.asInstanceOf[A]
		case Sym(_) => ???
		case Apply(_) => ???
	  }
	}
}
```

---

# Context

```scala

final class Context(value: Map[Expr.Sym, Any]) {

  def lookup[A](sym: Expr.Sym): Option[A] =
	value.get(sym).map(_.asInstanceOf[A])

}
```

```scala
new Context(Map(
  Expr.plus -> ((x: Double, y: Double) => x + y)
 )
```

--

## Symbol case

```scala
  case sym @ Expr.Sym(_) => context
	  .lookup[A](sym)
	  .map(Right(_))
	  .getOrElse(Left(sym))
```

---

# Apply case

```scala
  case Apply(sym, args) => {
	val len = args.length
	len match {
	  case 1 =>
		for {
		  f <- apply[Any => Any](sym, context)
		  a <- apply[Any](args(0), context)
		} yield f(a).asInstanceOf[A]
	  case 2 =>
		for {
		  f <- apply[(Any, Any) => Any](sym, context)
		  a <- apply[Any](args(0), context)
		  b <- apply[Any](args(1), context)
		} yield f(a, b).asInstanceOf[A]
	  }
  }
```
---

# Demo

---

# Type systems

- Set of rules which allow correct programs
- As expressive as needed

--

.center[**Let's do it through trial and error**]

```scala
object Typer {

  type Result = Either[(String, Expr), Type]

  def apply(expr: Expr): Result = ???
}
```

---

# Do fish have legs?

```lisp
(lt 3 (legs pet))
```

```scala
Apply(
  Sym("lt"),
  List(
	Lit.Num(3),
	Apply(
	  Sym("legs"),
	  List(Expr.Sym("pet"))
	)
  )
)
```

---

# Types

```scala
object Type {
  case object Num extends Type
  case object Str extends Type
  case object Bool extends Type
  case class Func(args: List[Type], ret: Type) extends Type
}
```

---

# Simplest type system

```scala
def apply(expr: Expr, context: Context): Result = {
	expr match {
	  case Lit.Num(_) => Result.success(Type.Num)
	  case Lit.Str(_) => Result.success(Type.Str)
	  case sym @ Expr.Sym(symbol) => ???
	  case Apply(_) => ???
	}
}
```

--

.center[**This looks familiar**]

---

# Symbol case

```scala
  case sym @ Expr.Sym(symbol) =>
	context
	  .lookup(sym)
	  .map(Result.success _)
	  .getOrElse(Result.fail(s"could not find $symbol in context", expr))
```

---

# Type system

## Rule 1
A function needs to be applied to arguments of the correct type and length

```lisp
(f x y)
```
```scala
Apply(
  Sym("f"),
  List(
	x,
	y
  )
)
```

- `f: (A, B) => C`
- `x: A`
- `y: B`

---

# Restrictive type systems

- What type is `pet`?

--

- We want to restrict bad programs
- But our type system doesn't allow correct programs

--

## Solution

 - Make a more powerful type system

```scala
case class Coprod(types: Set[Type]) extends Type
case class Record(name: String) extends Type
```

 - `pet: Dog | Cat | Fish`

---

# Coproduct rules

A type *A* is considered a **subtype** of another type *B*, **A < B**

### Case 1

If *A* = *B*

### Case 2
- If *B* = *B<sub>1</sub>* | *B<sub>2</sub>* | ...
- such that *A* = *B<sub>n</sub>*

### Case 3
- If *A* = *A<sub>1</sub>* | *A<sub>2</sub>* | ...
- and *B* = *B<sub>1</sub>* | *B<sub>2</sub>* | ...
- such that all *A<sub>i</sub>* = *B<sub>j</sub>*

---

# Rule 1 (revised)

A function needs to be applied to arguments of the correct type, or a **subtype** of
the type

```lisp
(f x y)
```

```scala
Apply(
  Sym("f"),
  List(
	x,
	y
  )
)
```

- `f: (A, B) => C`
- `x: A` or `x: D` where `D < A`
- `y: B` or `y: E` where `E < B`

---

# More complications

What should the following program return?

```scala
Apply(
  Sym("if"),
  x,
  List(
	Lit.Str("moo"),
	Lit.Num(3)
  )
)
```

--

## Solution

.center[**Make a more powerful type system**]

---

# Unions

### Case 1

 - If *A* and *B* are not coproducts
 - the union of *A*, *B* is *A | B* if *A* != *B*
 - otherwise, *A* if *A* = *B*

### Case 2
- If *A* and *B* are coproducts
- the union of *A*, *B* is *A<sub>1</sub> | ... | B<sub>1</sub> | ...*

### Case 3
- If one of them is a coproduct, *C*, and the other is not, *D*
- then the union of *A*, *B* is *C | D<sub>1</sub> | ... *

---

# Rule 2

In an if statement, the return of the if statement is the union of
the two types.

```scala
Apply(
  Sym("if"),
  x,
  List(
	Lit.Str("moo"),
	Lit.Num(3)
  )
)
```

- `(if cond a b)`
- `a: A`
- `b: B`
- `(if cond a b): A | B`

---

# Further complications

```scala
Apply(
  Sym("lt"),
  List(
	Lit.Num(3),
	Apply(
	  Sym("legs"),
	  List(Expr.Sym("pet"))
	)
  )
)
```

 - `legs: (Dog | Cat | Fish) => Int` or `legs: (Dog | Cat) => Int`
 - `pet: Dog | Cat | Fish`

--

.center[**Clearly not going to work**]

--

## Solution

 - Make a more powerful type system

---

# Is statement

```scala
Apply(
  Sym("is"),
  List(
	Sym("pet"),
	Type("Fish")
  )
)
```

--

- `(is pet 'Fish): Bool`
- If the *runtime* value = `true`, `pet: Fish`
- If the *runtime* value = `false`, `pet: !Fish`

.center[**What about compile time?**]

---

# Consider if statements

```scala
Apply(
  Sym("if"),
  List(
	Apply(
	  Sym("is"),
	  List(
		Sym("pet"),
		Type("Fish")
	  )
	),
	Lit.Str("pet should be fish"),
	Lit.Str("pet should be anything else")
  )
)
```

--

 - What about `not`, `and`, `or`?

--

## Solution

 - Make a more powerful type system

---

# Bounds

 - What should go into `a`?

```scala
Apply(
  Sym("is"),
	List(
	  a,
	  b
	)
)
```

## B type

 - What about `b`?

--

## Further constraints

 - Clearly `b: Type(B)`, where `B` is a valid type
 - One further constraint; `a: A`, then `A < B`

---

# Bounds (continued)

 - We then need to change the inferred type.
 - Same type of context as before

```scala
final class Context(value: Map[Expr, Type]) {
  val lookup = value.get _
}
```

--

```scala
final class Bound(val values: Map[Expr, Type]) {
  def complement(typer: Expr => Result[Type]): Bound = ???
}

```

```scala
type Result = Either[(String, Expr), (Type, Bound)]
```

 - `complement` is `!` from before
 - Define `context.plus(bound)`

---

## Operations (revisited)

### If statements

 - `(if a b c)`
 - `a: Boolean, B` where `B` is a bound
 - `b`, apply `B`
 - `c`, apply `!B`

--

.center[**Similar for other boolean operations**]

---

## Finally we can typecheck
```lisp
(if (is pet Fish) 1.0 (legs pet))
```
```scala
Apply(
  Sym("if"),
  List(
	Apply(
	  Sym("is"),
	  List(
		Sym("pet"),
		Type("Fish")
	  )
	),
	Lit.Num(1.0),
	Apply(
	  Sym("legs"),
	  List(
		Sym("pet")
	  )
	)
  )
)
```
---

# Demo

---

## Recap

 - Created an AST
 - Created an evaluator which could run a program
 - Created a simple typer
 - Made it more powerful

---

## Go forth and scheme!

### Evaluation

 - Really easy to do simple evaluators
 - You can play around with evaluation/compilation strategies

### Type systems

 - Are as simple as you want
 - Are as expressive as you want

---

## Q & A
