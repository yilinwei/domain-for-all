import Dependencies._

ThisBuild / scalaVersion     := "2.13.0"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "gitlab.yilinwei"
ThisBuild / organizationName := "yilinwei"

val root = (project in file("."))
  .settings(
    name := "domain-for-all",
    libraryDependencies += scalaTest % Test
  )

initialCommands in console := """import domain._"""

val scalacOptions = Seq("-deprecation")

licenses += "GPLv3" -> url("https://www.gnu.org/licenses/gpl-3.0.html")
